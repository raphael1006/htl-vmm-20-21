stages:
    - docker-build
    - check
    - build
    - test
    - release
    - deploy

.docker-build:
    image: docker:20.10.6
    stage: docker-build
    services:
        - name: docker:20.10.6-dind
          entrypoint: [ "env", "-u", "DOCKER_HOST" ]
          command: [ "dockerd-entrypoint.sh" ]
    variables:
        DOCKER_HOST: tcp://docker:2375/
        DOCKER_DRIVER: overlay2
        DOCKER_TLS_CERTDIR: ""
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

.rust-default:
    image: registry.gitlab.com/tobip64/rust-ci-docker-image:latest
    cache:
        key: $CI_JOB_NAME
        paths: [target/]
    interruptible: true
    timeout: 15m
    parallel:
        matrix:
            - CHANNEL: [+stable, +beta, +nightly]
    rules:
        - if: $CHANNEL == "+stable"
          allow_failure: false
        - allow_failure: true
    before_script:
        - rustup --version && rustc --version && cargo --version

.test-env-setup:
    extends: .rust-default
    image: $CI_REGISTRY_IMAGE/ci-test-e2e:latest
    stage: test
    needs: [ build ]
    before_script:
        - rustup --version && rustc --version && cargo --version
        - cp target/debug/libvmm.so .
        - mkdir cert
        - openssl req -x509 -newkey rsa:4096 -nodes -keyout cert/key.pem -out cert/cert.pem -days 30 -subj '/CN=localhost'
        - /usr/bin/net-services &

.deploy:
    stage: deploy
    needs: [build, "release:registry", "release:docker"]
    image: registry.gitlab.com/tobip64/rust-ci-docker-image:latest
    retry: 2
    timeout: 10m
    variables:
        GIT_STRATEGY: none
    script:
        - cp $CI_PROJECT_DIR/target/release/libvmm.so $CI_PROJECT_DIR
        - tar -cf data.tar libvmm.so config.toml frontend/
        - 'curl -T data.tar -H "Content-Type: application/tar" $HOST_DEPLOY_DOMAIN/$CI_PROJECT_NAME/$CI_ENVIRONMENT_SLUG/$CI_COMMIT_SHA'

docker:ci-test-e2e:
    extends: .docker-build
    script:
        - docker build -t $CI_REGISTRY_IMAGE/ci-test-e2e:latest -f Dockerfile_ci-test-e2e .
        - docker push $CI_REGISTRY_IMAGE/ci-test-e2e:latest
    rules:
        - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
          when: never
        - if: $DOCKER_BUILD
        - changes:
              - ./Dockerfile_ci-test-e2e

test:
    extends: .rust-default
    stage: check
    script:
        - cargo $CHANNEL test --verbose --all-features --no-fail-fast -- -Z unstable-options --format json | cargo2junit > results.xml
    artifacts:
        when: always
        reports:
            junit:
                - results.xml

bench:
    extends: .rust-default
    stage: check
    script:
        - cargo $CHANNEL bench --verbose
    artifacts:
        when: always
        reports:
            metrics: metrics.txt

clippy:
    extends: .rust-default
    stage: check
    script:
        - cargo $CHANNEL clippy --verbose --all-targets --all-features --message-format=json | gitlab-clippy > gl-code-quality-report.json
    artifacts:
        when: always
        reports:
            codequality: gl-code-quality-report.json

check:fmt:
    extends: .rust-default
    stage: check
    parallel:
    script:
        - cargo fmt -- --check
    allow_failure: true
    rules:
        - if: $RUN_RUST_FMT

check:audit:
    extends: .rust-default
    stage: check
    parallel:
    script:
        - cargo audit

build:
    extends: .rust-default
    stage: build
    needs: [test, clippy, "check:audit"]
    timeout: 1h
    script:
        - cargo $CHANNEL build --verbose
        - cargo $CHANNEL build --release --verbose
    artifacts:
        paths:
            - target/debug/libvmm.so
            - target/release/libvmm.so
            - frontend/
            - docker/
            - config.toml

build:docs:
    extends: .rust-default
    stage: build
    needs: [test, clippy, "check:audit"]
    parallel:
    script:
        - cargo +nightly doc --no-deps
    artifacts:
        paths:
            - target/doc

test-e2e:
    extends: .test-env-setup
    script:
        - cargo $CHANNEL test --verbose --no-fail-fast -- -Z unstable-options --format json | cargo2junit > results.xml
    artifacts:
        reports:
            junit:
                - results.xml

bench-e2e:
    extends: .test-env-setup
    script:
        - cargo $CHANNEL bench --verbose
    artifacts:
        reports:
            metrics: metrics.txt

release:pages:
    stage: release
    image: alpine:latest
    needs: [ test-e2e, "build:docs" ]
    timeout: 10m
    variables:
        GIT_STRATEGY: none
    script:
        - rm -rf public/
        - mv target/doc public/
        - echo '<meta http-equiv="refresh" content="0; url={{ LIBRARY NAME }}">' > public/index.html
    artifacts:
        paths:
            - public/
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG

release:docker:
    extends: .docker-build
    stage: release
    needs: [ test-e2e, build ]
    script:
        - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG $([[ $CI_COMMIT_TAG ]] && echo -n "-t $CI_REGISTRY_IMAGE:latest" || echo -n "") -f Dockerfile_minimal .
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
        - '[[ $CI_COMMIT_TAG ]] && docker push $CI_REGISTRY_IMAGE:latest || echo -n ""'
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG
        - if: $CI_MERGE_REQUEST_ID
          when: manual

release:registry:
    stage: release
    image: registry.gitlab.com/tobip64/rust-ci-docker-image:latest
    needs: [ test-e2e, build ]
    timeout: 10m
    variables:
        GIT_STRATEGY: none
        PACKAGE_NAME: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA.tar.gz
    script:
        - cp $CI_PROJECT_DIR/target/release/libvmm.so $CI_PROJECT_DIR
        - tar -czvf $PACKAGE_NAME libvmm.so config.toml frontend/
        - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_NAME "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_COMMIT_REF_SLUG}/${CI_COMMIT_SHA}/${PACKAGE_NAME}"'
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG
        - if: $CI_MERGE_REQUEST_ID
          when: manual

release:gitlab:
    stage: release
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    needs: [test-e2e]
    timeout: 10m
    variables:
        GIT_STRATEGY: none
        PACKAGE_NAME: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA.tar.gz
    script:
        - release-cli create \
          --name $CI_COMMIT_TAG \
          --description $CI_COMMIT_MESSAGE \
          --tag-name $CI_COMMIT_TAG \
          --ref $CI_COMMIT_SHA \
          --assets-link '{"name":"Package","url":"${CI_REPOSITORY_URL}/packages/generic/${CI_COMMIT_REF_SLUG}/${CI_COMMIT_SHA}/${PACKAGE_NAME}","link_type":"package"}' \
          --assets-link '{"name":"Docker Image","url":"${CI_REGISTRY_IMAGE}:$CI_COMMIT_REF_SLUG","link_type":"image"}' \
          --assets-link '{"name":"Docs","url":""}'
    release:
        name: $CI_COMMIT_TAG
        description: './CHANGELOG.md'
        tag_name: $CI_COMMIT_TAG
        ref: $CI_COMMIT_SHA
    rules:
        - if: $CI_COMMIT_TAG
          when: manual

deploy:dev:
    extends: .deploy
    environment:
        name: development
        url:  $CI_PROJECT_NAME-dev.$HOST_DOMAIN
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:prod:
    extends: .deploy
    environment:
        name: production
        url: $CI_PROJECT_NAME.$HOST_DOMAIN
    rules:
        - if: $CI_COMMIT_TAG

deploy:review:
    extends: .deploy
    environment:
        name: review/$CI_COMMIT_REF_NAME
        url: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.$HOST_DOMAIN
        on_stop: cleanup:review
    rules:
        - if: $CI_MERGE_REQUEST_ID

cleanup:review:
    extends: .deploy
    environment:
        name: review/$CI_COMMIT_REF_NAME
        action: stop
    script:
        - ENV_ROOT=/root/$CI_PROJECT_NAME/$CI_ENVIRONMENT_SLUG
        - SERVER=web-server@$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG
        - curl -X "DELETE" "$HOST_DEPLOY_DOMAIN/$CI_PROJECT_NAME/$CI_ENVIRONMENT_SLUG/$CI_COMMIT_SHA"
    rules:
        - if: $CI_MERGE_REQUEST_ID
