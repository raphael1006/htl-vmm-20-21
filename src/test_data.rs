// MIT License
//
// Copyright (c) 2020-2021 Tobias Pfeiffer <t.pfeiffer@htlstp.at>
// Copyright (c) 2020-2021 Samuel Braunauer <s.braunauer@htlstp.at>
// Copyright (c) 2020-2021 Philip Ebner <p.ebner@htlstp.at>
// Copyright (c) 2020-2021 Raphael Loescher <r.loescher@htlstp.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#[test]
#[ignore]
fn setup() {
	use super::*;
	use std::time::Duration;
	
	let c = ConnectionOptions {
		hostname: Some("49.12.74.28".to_string()),
		timeout: Some(Duration::from_secs(5)),
		..ConnectionOptions::default()
	}.connect().unwrap();
	
	db_drop("vmm-dev")
		.run::<reql::WriteResult>(&c, None);
	
	db_create("vmm-dev")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("lbvo_categories")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("subjects")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("classes")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("users")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("groups")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("categories")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	db("vmm-dev")
		.table_create_db("audit_trail")
		.run::<reql::WriteResult>(&c, None)
		.unwrap();
	
	let lbvo_category_ids = db("vmm-dev")
		.table("lbvo_categories")
		.insert_seq([
			obj! { short_name: "GLF", name: "Grafische Leistungsfeststellungen" },
			obj! { short_name: "MIU", name: "Mitarbeit im Unterricht" },
			obj! { short_name: "MPF", name: "Mündliche Prüfung" },
			obj! { short_name: "MUB", name: "Mündliche Übungen" },
			obj! { short_name: "PLF", name: "Praktische Leistungsfestellungen" },
			obj! { short_name: "SUP", name: "Schriftliche Überprüfungen" },
			obj! { short_name: "SUA", name: "Schularbeiten" }
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	let subject_ids = db("vmm-dev")
		.table("subjects")
		.insert_seq([
			obj! { short_name: "POS", name: "Programmieren", competences: ["OOP", "FP"] },
			obj! { short_name: "AM", name: "Mathematik", competences: ["Wahrscheinlichkeitsrechnung", "Statistik", "Geometrie"] },
			obj! { short_name: "NW", name: "Naturwissenschaften", competences: ["Physik", "Chemie", "Biologie"] },
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	let students = db("vmm-dev")
		.table("users")
		.insert_seq([
			obj! { external: "20170009", ty: 2, name: "Braunauer Samuel", schug82e: false },
			obj! { external: "20170014", ty: 2, name: "Ebner Philip",     schug82e: true },
			obj! { external: "20170043", ty: 2, name: "Löscher Raphael",  schug82e: true },
			obj! { external: "20170061", ty: 2, name: "Pfeiffer Tobias",  schug82e: true },
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	let classes = db("vmm-dev")
		.table("classes")
		.insert_seq([
			obj! { name: "4CHIF", students: students.as_slice() },
			obj! { name: "4BHIF", students: [] as [(); 0] },
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	let teachers = db("vmm-dev")
		.table("users")
		.insert_seq([
			obj! { external: "reio",     ty: 1, name: "Otto Reichel",        subjects_classes: [[subject_ids[1], classes[0]], [subject_ids[0], classes[0]]] },
			obj! { external: "scre",     ty: 1, name: "Christoph Schreiber", subjects_classes: [[subject_ids[1], classes[0]], [subject_ids[0], classes[0]]] },
			obj! { external: "hoec",     ty: 1, name: "Johann Höchtl",       subjects_classes: [[subject_ids[2], classes[0]]] },
			obj! { external: "teacher",  ty: 1, name: "Test Teacher",        subjects_classes: [[subject_ids[0], classes[0]], [subject_ids[1], classes[0]], [subject_ids[2], classes[0]]] },
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	let categories = db("vmm-dev")
		.table("categories")
		.insert_seq([
			obj! {
				name: "Test Category",
				owner: teachers[3],
				subjects: [] as [(); 0],
				public: false,
				lbvo: lbvo_category_ids[5],
				weight: 0.25,
				weight_time: 0.0,
				weight_max: 0.5,
				input: 1,
				output: 0,
				calc: 0,
				ranges: [100.0, 87.5, 75.0, 62.5, 50.0]
			},
			obj! {
				name: "REIO's Cat",
				owner: teachers[0],
				subjects: [subject_ids[0]],
				public: true,
				lbvo: lbvo_category_ids[1],
				weight: 0.25,
				weight_max: 0.5,
				input: 0,
				output: 1,
				calc: 0,
				ranges: [100.0, 87.5, 75.0, 62.5, 50.0]
			}
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap()
		.generated_keys;
	
	db("vmm-dev")
		.table("groups")
		.insert_seq([
			obj! {
					name:     "4CHIF_POS1",
					owner:    teachers[3],
					subject:  subject_ids[0],
					teachers: [teachers[0]],
					students: [students[0], students[1], students[2], students[3]],
					grades:   [
						obj! {
							teacher: teachers[3],
							student: students[3],
							grades:  [
								obj! { id: uuid(), date: iso8601("2021-01-01T00:00:00+00:00"), edited: iso8601("2021-01-01T00:00:00+00:00"), category: categories[0], value: "100", comp_passed: 1, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-02T00:00:00+00:00"), edited: iso8601("2021-01-02T00:00:00+00:00"), category: categories[1], value: "2",   comp_passed: 2, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-03T00:00:00+00:00"), edited: iso8601("2021-01-03T00:00:00+00:00"), category: categories[0], value: "69",  comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-05T00:00:00+00:00"), edited: iso8601("2021-01-05T00:00:00+00:00"), category: categories[0], value: "83",  comp_passed: 0, comp_failed: 1 },
								obj! { id: uuid(), date: iso8601("2021-01-08T00:00:00+00:00"), edited: iso8601("2021-01-08T00:00:00+00:00"), category: categories[1], value: "1",   comp_passed: 1, comp_failed: 0 }
							]
						},
						obj! {
							teacher: teachers[3],
							student: students[0],
							grades:  [
								obj! { id: uuid(), date: iso8601("2021-01-01T00:00:00+00:00"), edited: iso8601("2021-01-01T00:00:00+00:00"), category: categories[0], value: "56", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-02T00:00:00+00:00"), edited: iso8601("2021-01-02T00:00:00+00:00"), category: categories[1], value: "3",  comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-03T00:00:00+00:00"), edited: iso8601("2021-01-03T00:00:00+00:00"), category: categories[0], value: "95", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-05T00:00:00+00:00"), edited: iso8601("2021-01-05T00:00:00+00:00"), category: categories[0], value: "62", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-08T00:00:00+00:00"), edited: iso8601("2021-01-08T00:00:00+00:00"), category: categories[1], value: "4",  comp_passed: 0, comp_failed: 0 }
							]
						},
						obj! {
							teacher: teachers[3],
							student: students[1],
							grades:  [
								obj! { id: uuid(), date: iso8601("2021-01-01T00:00:00+00:00"), edited: iso8601("2021-01-01T00:00:00+00:00"), category: categories[0], value: "85", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-02T00:00:00+00:00"), edited: iso8601("2021-01-02T00:00:00+00:00"), category: categories[1], value: "3",  comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-03T00:00:00+00:00"), edited: iso8601("2021-01-03T00:00:00+00:00"), category: categories[0], value: "87", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-05T00:00:00+00:00"), edited: iso8601("2021-01-05T00:00:00+00:00"), category: categories[0], value: "54", comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-08T00:00:00+00:00"), edited: iso8601("2021-01-08T00:00:00+00:00"), category: categories[1], value: "2",  comp_passed: 0, comp_failed: 0 }
							]
						},
						obj! {
							teacher: teachers[3],
							student: students[2],
							final_grade_override: 2.5,
							grades:  [
								obj! { id: uuid(), date: iso8601("2021-01-01T00:00:00+00:00"), edited: iso8601("2021-01-01T00:00:00+00:00"), category: categories[0], value: "60" , comp_passed: 1, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-02T00:00:00+00:00"), edited: iso8601("2021-01-02T00:00:00+00:00"), category: categories[1], value: "5",   comp_passed: 1, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-03T00:00:00+00:00"), edited: iso8601("2021-01-03T00:00:00+00:00"), category: categories[0], value: "75",  comp_passed: 0, comp_failed: 0 },
								obj! { id: uuid(), date: iso8601("2021-01-05T00:00:00+00:00"), edited: iso8601("2021-01-05T00:00:00+00:00"), category: categories[0], value: "30",  comp_passed: 0, comp_failed: 2 },
								obj! { id: uuid(), date: iso8601("2021-01-08T00:00:00+00:00"), edited: iso8601("2021-01-08T00:00:00+00:00"), category: categories[1], value: "4",   comp_passed: 0, comp_failed: 0 }
							]
						},
					]
				},
			obj! {
					name:     "4CHIF_POS2",
					owner:    teachers[1],
					subject:  subject_ids[0],
					teachers: [teachers[3]],
					students: [students[2], students[1]],
					grades:   [] as [(); 0]
				}
		])
		.run::<reql::WriteResult>(&c, None)
		.unwrap()
		.next()
		.unwrap()
		.unwrap();
}