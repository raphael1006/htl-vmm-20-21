# htl-vmm

## Builds

- [Binary](https://gitlab.com/TobiP64/htl-vmm/-/packages)
- [Docker container](https://gitlab.com/TobiP64/htl-vmm/container_registry)

## Setup

- [Install RethinkDB](https://rethinkdb.com/docs/install/)
- Install OpenLDAP
- [Install net-services](https://gitlab.com/TobiP64/net-services)

### Telemetry (optional)

- [OpenTelemetry Collector](https://opentelemetry.io/docs/collector/getting-started/)
- [APM Server](https://www.elastic.co/downloads/apm)
- [Elasticsearch](https://www.elastic.co/downloads/elasticsearch)
- [Grafana](https://grafana.com/grafana/download?pg=get&plcmt=selfmanaged-box1-cta1)

## Configuration

config.toml:

| Field            | Description
|------------------|---
| `vmm.db`         | URL to a RethinkDB instance
| `vmm.ldap`       | URL to an LDAP server
| `ldap-mech`      | Specifies which mechanism to use when authenticating a user with LDAP
| `ldap-dn-suffix` | Specifies a string that is appended to the username to form a valid LDAP DN

## API

### `/api/login`

Login a user.

#### `POST` Request

| Field | Description | Type   | Optional
|-------|-------------|--------|---------
| `usr` | Username    | String | No
| `pwd` | Password    | String | No
| `rem` | Remember me | Bool   | Yes

Example:
```json
{
    "usr": "teacher",
    "pwd": "test123",
    "rem": true
}
```

##### `POST` Response

| Status    | Meaning
|-----------|--------
| OK        | Authentication successful
| FORBIDDEN | Invalid Credentials

| Cookie  | Path   | Description
|---------|--------|------------
| `token` | `/api/`| 64 random bytes used to authenticate the user to the server

### `/api/logout`

Logout a user.

#### `POST` Request

This request does not have a body.

#### `POST` Response

| Status    | Meaning
|-----------|--------
| OK        | Logout successful
| FORBIDDEN | Invalid or no token supplied (See `/api/login` `POST` Response Cookies)

### `/api/resume`

Resume a user's session and retrieve the user's data.

#### `POST` Request

This request does not have a body.

#### `POST` Response

| Field      | Type   | Optional | Description 
|------------|--------|----------|-------------
| `id`       | String | No       | The user's UUID
| `ty`       | Int    | No       | The users Type, 0 for admin, 1 for teacher, 2 for student
| `external` | String | No       | The user's LDAP name
| `name`     | String | No       | The user's name
| `img`      | String | Yes      | The user's profile picture
| ...        | -      | -        | All other fields are dependent on the user type

Example:

```json
{
	"id": "9aa01dd8-9e4c-497a-ac23-ffb29914b815",
	"ty": 1,
	"external": "teacher",
    "name": "Test Teacher"
}
```

### `/api/group`

Update a group.

#### Request

| Parameter | Description
|-----------|------------
| id        | The group's id

#### Response

| Status    | Meaning
|-----------|--------
| OK        | Group updated successfully
| FORBIDDEN | User is not authorized to edit the group

### `/api/category`

Update a category.

#### Request

| Parameter | Description
|-----------|------------
| id        | The category's id

#### Response

| Status    | Meaning
|-----------|--------
| OK        | Category updated successfully
| FORBIDDEN | User is not authorized to edit the category

## Administrator's Guide

### Backup and Recovery

See https://rethinkdb.com/docs/backup/.

TL;DR: Use `rethinkdb dump` for backups and `rethinkdb restore` for recovery. 

Since realistically only groups and categories are updated regularly, limiting backup to these tables is recommended.

### Manage Data

Open `localhost:8080` (on the server) and navigate to `Data Explorer`.

### Admin page

Login with the provided admin credentials.
